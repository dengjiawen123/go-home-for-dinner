import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/shop',
    name: 'Shop',
    component: () => import('../views/shop.vue')
  },
  {
    path: '/collect',
    name: 'Collect',
    component: () => import('../views/collect.vue')
  },
  {
    path: '/my',
    name: 'My',
    component: () => import('../views/my.vue')
  },
  {
    path: '/cook',
    name: 'Cook',
    component: () => import('../components/cookContent.vue')
  },
  {
    path: '/search',
    name: 'Search',
    component: () => import('../components/myhome/search.vue')
  },
  {
    path: '/newhot',
    name: 'newhot',
    component: () => import('../views/newhot.vue')
  },
  {
    path: '/note',
    name: 'Note',
    component: () => import('../views/noteCon.vue')
  }
  
]

const router = new VueRouter({
  routes
})

export default router
